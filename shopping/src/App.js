import Header from "./components/Header";
import Footer from "./components/Footer";
import Items from "./components/items";
import Categories from "./components/Categories";
import ShowFullItem from "./components/ShowFullItem";
import React, {useState} from 'react';
import useChooseCategory from "./components/useChooseCategory";

const items = [{
    id: 1, img: 'gamer-chair.png', title: 'Стул игровой', desc: 'Новый стул от dxracer', category: 'chairs', price: '50'
}, {
    id: 2,
    img: 'gamer-table.jpg',
    title: 'Стол геймерский',
    desc: 'Новая иновация фаннеры',
    category: 'table',
    price: '150'
}, {
    id: 3,
    img: 'sofa.jpg',
    title: 'Диван',
    desc: 'удобный для любого дружного колектива',
    category: 'sofa',
    price: '350'
}, {
    id: 4,
    img: 'lamps.jpeg',
    title: 'Надстольная лампа',
    desc: 'удобная лампа для ночных тус',
    category: 'lamps',
    price: '25'
}, {
    id: 5, img: 'chair.jpeg', title: 'табуретка', desc: 'обычная удобная табуретка', category: 'chairs', price: '15'
},]
const App = () => {
    const [orders, setOrders] = useState([]);
    const {currentCategory, chooseCategory} = useChooseCategory(items);
    const [currentItems, setCurrentItems] = useState(items);
    const [showFullItem, setShowFullItem] = useState(false);
    const [fullItem, setFullItem] = useState({});

    const addToOrder = (item) => {
        const isInArray = orders.some((el) => el.id === item.id);
        if (!isInArray) {
            setOrders((prevOrders) => [...prevOrders, item]);
        }
    };

    const deleteOrder = (id) => {
        setOrders((prevOrders) => prevOrders.filter((el) => el.id !== id));
    };


    const onShowItem = (item) => {
        setFullItem(item);
        setShowFullItem(!showFullItem);
    };


    return (<div className="wrapper">
            <Header orders={orders} onDelete={deleteOrder}/>
            <Categories currentCategory={chooseCategory()}/>
            <Items onShowItem={onShowItem} items={currentItems} onAdd={addToOrder}/>
            {showFullItem && <ShowFullItem onAdd={addToOrder} onShowItem={onShowItem} item={fullItem}/>}
            <Footer/>
        </div>);
};


export default App;
