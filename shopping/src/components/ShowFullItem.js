import React from 'react';

const ShowFullItem = (props) => {
    return (
        <div className={"full-item"}>
            <div>
                <p className={'close-order'}>X</p>
                <img className={'full-item-screen'} src={"./img/" + props.item.img} onClick={() => props.onShowItem(props.item)} alt=""/>
                <h2>{props.item.title}</h2>
                <p>{props.item.desc}</p>
                <b>{props.item.price}$</b>
                <div className='add-to-card' onClick={() => props.onAdd(props.item)}>+</div>
            </div>
        </div>
    );
};

export default ShowFullItem;
