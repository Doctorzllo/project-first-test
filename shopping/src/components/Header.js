import React, {useState} from 'react';
import {FaShopify} from "react-icons/fa";
import Order from "./Order";
const showOrders = (props) =>{
let summa = 0;
props.orders.forEach(el => summa += Number.parseFloat(el.price))
    return(
        <div>
            {props.orders.map(el =>(
                <Order onDelete={props.onDelete} key={el.id} item={el}/>
            ))}
            <p className="summa">Сумма {new Intl.NumberFormat().format(summa)}$</p>
        </div>
    )
}
const showNothing = () =>{
    return(
        <div className="empty">
            <h2>Товаров нету</h2>
        </div>
    )
}
export default function Header(props) {
    const [isActive, setIsActive] = useState(false);
    return (
        <header>
            <div className={"header-bar"}>
                <span className="logo">House staff</span>
                <ul className="nav">
                    <li>Про нас</li>
                    <li>Контакты</li>
                    <li>Кабинеты</li>
                </ul>
                <FaShopify id={"shop-card-button"} className={isActive ? 'active' : ''}
                           onClick={() => setIsActive(!isActive)}/>
                {isActive && (
                    <div className="shopCard">
                        {props.orders.length> 0 ? showOrders(props) : showNothing()}
                    </div>
                )}
            </div>
            <div className="presentation">

            </div>
        </header>

    )
}