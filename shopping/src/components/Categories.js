import React from 'react';

const Categories = ({ chooseCategory }) => {
    const categories = [
        {
            key: 'all',
            name: 'Все',
        },
        {
            key: 'chairs',
            name: 'стулья',
        },
        {
            key: 'table',
            name: 'столы',
        },
        {
            key: 'sofa',
            name: 'диваны',
        },
        {
            key: 'lamps',
            name: 'свет',
        },
    ];

    return (
        <div className="categories">
            {categories.map((el) => (
                <div key={el.key} onClick={() => chooseCategory(el.key)}>
                    {el.name}
                </div>
            ))}
        </div>
    );
};

export default Categories;
