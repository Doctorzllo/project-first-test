import React, { useState } from 'react';

const useChooseCategory = (items) => {
    const [currentItems, setCurrentItems] = useState(items);

    const chooseCategory = (category) => {
        if (category === 'all') {
            setCurrentItems(items);
        } else {
            setCurrentItems(items.filter((el) => el.category === category));
        }
    };

    return { currentItems, chooseCategory };
};

export default useChooseCategory;
